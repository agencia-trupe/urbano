<?php

use Illuminate\Database\Seeder;

class ServicosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicos')->insert([
            [
                'titulo' => 'Captação e Definição do Produto',
                'slug'   => 'captacao-e-definicao-do-produto',
                'ordem'  => 0
            ],
            [
                'titulo' => 'Aprovação do Projeto',
                'slug'   => 'aprovacao-do-projeto',
                'ordem'  => 1
            ],
            [
                'titulo' => 'Comercialização do Empreendimento',
                'slug'   => 'comercializacao-do-empreendimento',
                'ordem'  => 2
            ]
        ]);
    }
}
