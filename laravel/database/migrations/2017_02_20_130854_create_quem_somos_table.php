<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->text('chamada');
            $table->text('texto');
            $table->text('chamada_contato');
            $table->text('texto_contato');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos');
    }
}
