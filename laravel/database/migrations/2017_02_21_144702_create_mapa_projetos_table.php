<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapaProjetosTable extends Migration
{
    public function up()
    {
        Schema::create('mapa_projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('aracaju');
            $table->text('aracaju_texto');
            $table->boolean('belo_horizonte');
            $table->text('belo_horizonte_texto');
            $table->boolean('belem');
            $table->text('belem_texto');
            $table->boolean('boa_vista');
            $table->text('boa_vista_texto');
            $table->boolean('brasilia');
            $table->text('brasilia_texto');
            $table->boolean('campo_grande');
            $table->text('campo_grande_texto');
            $table->boolean('cuiaba');
            $table->text('cuiaba_texto');
            $table->boolean('curitiba');
            $table->text('curitiba_texto');
            $table->boolean('florianopolis');
            $table->text('florianopolis_texto');
            $table->boolean('fortaleza');
            $table->text('fortaleza_texto');
            $table->boolean('goiania');
            $table->text('goiania_texto');
            $table->boolean('joao_pessoa');
            $table->text('joao_pessoa_texto');
            $table->boolean('macapa');
            $table->text('macapa_texto');
            $table->boolean('maceio');
            $table->text('maceio_texto');
            $table->boolean('manaus');
            $table->text('manaus_texto');
            $table->boolean('natal');
            $table->text('natal_texto');
            $table->boolean('palmas');
            $table->text('palmas_texto');
            $table->boolean('porto_alegre');
            $table->text('porto_alegre_texto');
            $table->boolean('porto_velho');
            $table->text('porto_velho_texto');
            $table->boolean('recife');
            $table->text('recife_texto');
            $table->boolean('rio_branco');
            $table->text('rio_branco_texto');
            $table->boolean('rio_de_janeiro');
            $table->text('rio_de_janeiro_texto');
            $table->boolean('salvador');
            $table->text('salvador_texto');
            $table->boolean('sao_luis');
            $table->text('sao_luis_texto');
            $table->boolean('sao_paulo');
            $table->text('sao_paulo_texto');
            $table->boolean('teresina');
            $table->text('teresina_texto');
            $table->boolean('vitoria');
            $table->text('vitoria_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('mapa_projetos');
    }
}
