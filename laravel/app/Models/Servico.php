<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;

class Servico extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 367,
                'height' => 225,
                'path'   => 'assets/img/servicos/'
            ],
            [
                'width'  => 720,
                'height' => 380,
                'path'   => 'assets/img/servicos/ampliado/'
            ],
            [
                'width'  => 100,
                'height' => 60,
                'path'   => 'assets/img/servicos/thumb/'
            ],
        ]);
    }

}
