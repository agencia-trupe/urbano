<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MapaProjetos extends Model
{
    protected $table = 'mapa_projetos';

    protected $guarded = ['id'];

}
