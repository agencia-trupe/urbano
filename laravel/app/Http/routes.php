<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quem-somos');
    Route::get('nossos-servicos/{servico_slug?}', 'ServicosController@index')->name('nossos-servicos');
    Route::get('projetos', 'ProjetosController@index')->name('projetos');
    Route::get('seja-nosso-parceiro', 'ParceiroController@index')->name('parceiro');
    Route::post('seja-nosso-parceiro', 'ParceiroController@post')->name('parceiro.post');
    Route::get('contato', 'ContatoController@index')->name('contato');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('mapa-projetos', 'MapaProjetosController', ['only' => ['index', 'update']]);
		Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');
		Route::resource('servicos', 'ServicosController');
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
