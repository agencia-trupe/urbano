<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index(Servico $servico)
    {
        $servicos = Servico::ordenados()->get();

        if ($servico->exists)
            return view('frontend.servicos.show', compact('servicos', 'servico'));

        return view('frontend.servicos.index', compact('servicos'));
    }
}
