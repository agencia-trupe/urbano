<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MapaProjetosRequest;
use App\Http\Controllers\Controller;

use App\Models\MapaProjetos;

class MapaProjetosController extends Controller
{
    public function index()
    {
        $registro = MapaProjetos::first();

        return view('painel.mapa-projetos.edit', compact('registro'));
    }

    public function update(MapaProjetosRequest $request, MapaProjetos $registro)
    {
        try {
            $input = $request->all();
            $registro->update($input);

            return redirect()->route('painel.mapa-projetos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
