<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;
use App\Models\MapaProjetos;

class ProjetosController extends Controller
{
    public function index()
    {
        $desenvolvimento = Projeto::ordenados()->whereStatus('Em Desenvolvimento')->get();
        $desenvolvido    = Projeto::ordenados()->whereStatus('Desenvolvido')->get();

        $marcadores = MapaProjetos::first();
        $estados    = ['aracaju', 'belo_horizonte', 'belem', 'boa_vista', 'brasilia', 'campo_grande', 'cuiaba', 'curitiba', 'florianopolis', 'fortaleza', 'goiania', 'joao_pessoa', 'macapa', 'maceio', 'manaus', 'natal', 'palmas', 'porto_alegre', 'porto_velho', 'recife', 'rio_branco', 'rio_de_janeiro', 'salvador', 'sao_luis', 'sao_paulo', 'teresina', 'vitoria'];

        return view('frontend.projetos', compact('desenvolvimento', 'desenvolvido', 'marcadores', 'estados'));
    }
}
