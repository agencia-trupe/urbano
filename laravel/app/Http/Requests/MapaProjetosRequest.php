<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MapaProjetosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'aracaju' => '',
            'aracaju_texto' => '',
            'belo_horizonte' => '',
            'belo_horizonte_texto' => '',
            'belem' => '',
            'belem_texto' => '',
            'boa_vista' => '',
            'boa_vista_texto' => '',
            'brasilia' => '',
            'brasilia_texto' => '',
            'campo_grande' => '',
            'campo_grande_texto' => '',
            'cuiaba' => '',
            'cuiaba_texto' => '',
            'curitiba' => '',
            'curitiba_texto' => '',
            'florianopolis' => '',
            'florianopolis_texto' => '',
            'fortaleza' => '',
            'fortaleza_texto' => '',
            'goiania' => '',
            'goiania_texto' => '',
            'joao_pessoa' => '',
            'joao_pessoa_texto' => '',
            'macapa' => '',
            'macapa_texto' => '',
            'maceio' => '',
            'maceio_texto' => '',
            'manaus' => '',
            'manaus_texto' => '',
            'natal' => '',
            'natal_texto' => '',
            'palmas' => '',
            'palmas_texto' => '',
            'porto_alegre' => '',
            'porto_alegre_texto' => '',
            'porto_velho' => '',
            'porto_velho_texto' => '',
            'recife' => '',
            'recife_texto' => '',
            'rio_branco' => '',
            'rio_branco_texto' => '',
            'rio_de_janeiro' => '',
            'rio_de_janeiro_texto' => '',
            'salvador' => '',
            'salvador_texto' => '',
            'sao_luis' => '',
            'sao_luis_texto' => '',
            'sao_paulo' => '',
            'sao_paulo_texto' => '',
            'teresina' => '',
            'teresina_texto' => '',
            'vitoria' => '',
            'vitoria_texto' => '',
        ];
    }
}
