(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('nav#mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            pagerTemplate: '<a href=#>{{slideNum}}</a>',
            timeout: 6000,
            speed: 750
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/seja-nosso-parceiro',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.projetosLightbox = function() {
        $('.fancybox').fancybox({
            padding: 4,
            maxHeight: '90%',
            maxWidth: '90%',
        });

        $('.projeto').click(function(e) {
            var el, id = $(this).data('galeria');
            if(id){
                el = $('.fancybox[rel=galeria' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        $('#form-contato').on('submit', this.envioContato);
        this.projetosLightbox();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
