@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco_matriz', 'Endereço Matriz') !!}
    {!! Form::textarea('endereco_matriz', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone_brasilia', 'Telefone Brasília') !!}
            {!! Form::text('telefone_brasilia', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('email_brasilia', 'E-mail Brasília') !!}
            {!! Form::email('email_brasilia', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone_saopaulo', 'Telefone São Paulo') !!}
            {!! Form::text('telefone_saopaulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('email_saopaulo', 'E-mail São Paulo') !!}
            {!! Form::email('email_saopaulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('facebook', 'Facebook') !!}
            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('instagram', 'Instagram') !!}
            {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
