@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Mapa Projetos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mapa-projetos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.mapa-projetos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
