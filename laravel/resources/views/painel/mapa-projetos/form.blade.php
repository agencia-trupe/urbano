@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('aracaju', 'Aracaju') !!}
    {!! Form::hidden('aracaju', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('aracaju', 1, null) !!}
        </div>
        {!! Form::textarea('aracaju_texto', null, ['id' => 'aracaju_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('belo_horizonte', 'Belo Horizonte') !!}
    {!! Form::hidden('belo_horizonte', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('belo_horizonte', 1, null) !!}
        </div>
        {!! Form::textarea('belo_horizonte_texto', null, ['id' => 'belo_horizonte_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('belem', 'Belém') !!}
    {!! Form::hidden('belem', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('belem', 1, null) !!}
        </div>
        {!! Form::textarea('belem_texto', null, ['id' => 'belem_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('boa_vista', 'Boa Vista') !!}
    {!! Form::hidden('boa_vista', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('boa_vista', 1, null) !!}
        </div>
        {!! Form::textarea('boa_vista_texto', null, ['id' => 'boa_vista_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('brasilia', 'Brasília') !!}
    {!! Form::hidden('brasilia', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('brasilia', 1, null) !!}
        </div>
        {!! Form::textarea('brasilia_texto', null, ['id' => 'brasilia_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('campo_grande', 'Campo Grande') !!}
    {!! Form::hidden('campo_grande', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('campo_grande', 1, null) !!}
        </div>
        {!! Form::textarea('campo_grande_texto', null, ['id' => 'campo_grande_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('cuiaba', 'Cuiabá') !!}
    {!! Form::hidden('cuiaba', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('cuiaba', 1, null) !!}
        </div>
        {!! Form::textarea('cuiaba_texto', null, ['id' => 'cuiaba_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('curitiba', 'Curitiba') !!}
    {!! Form::hidden('curitiba', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('curitiba', 1, null) !!}
        </div>
        {!! Form::textarea('curitiba_texto', null, ['id' => 'curitiba_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('florianopolis', 'Florianópolis') !!}
    {!! Form::hidden('florianopolis', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('florianopolis', 1, null) !!}
        </div>
        {!! Form::textarea('florianopolis_texto', null, ['id' => 'florianopolis_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('fortaleza', 'Fortaleza') !!}
    {!! Form::hidden('fortaleza', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('fortaleza', 1, null) !!}
        </div>
        {!! Form::textarea('fortaleza_texto', null, ['id' => 'fortaleza_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('goiania', 'Goiânia') !!}
    {!! Form::hidden('goiania', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('goiania', 1, null) !!}
        </div>
        {!! Form::textarea('goiania_texto', null, ['id' => 'goiania_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('joao_pessoa', 'João Pessoa') !!}
    {!! Form::hidden('joao_pessoa', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('joao_pessoa', 1, null) !!}
        </div>
        {!! Form::textarea('joao_pessoa_texto', null, ['id' => 'joao_pessoa_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('macapa', 'Macapá') !!}
    {!! Form::hidden('macapa', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('macapa', 1, null) !!}
        </div>
        {!! Form::textarea('macapa_texto', null, ['id' => 'macapa_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('maceio', 'Maceió') !!}
    {!! Form::hidden('maceio', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('maceio', 1, null) !!}
        </div>
        {!! Form::textarea('maceio_texto', null, ['id' => 'maceio_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('manaus', 'Manaus') !!}
    {!! Form::hidden('manaus', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('manaus', 1, null) !!}
        </div>
        {!! Form::textarea('manaus_texto', null, ['id' => 'manaus_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('natal', 'Natal') !!}
    {!! Form::hidden('natal', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('natal', 1, null) !!}
        </div>
        {!! Form::textarea('natal_texto', null, ['id' => 'natal_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('palmas', 'Palmas') !!}
    {!! Form::hidden('palmas', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('palmas', 1, null) !!}
        </div>
        {!! Form::textarea('palmas_texto', null, ['id' => 'palmas_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('porto_alegre', 'Porto Alegre') !!}
    {!! Form::hidden('porto_alegre', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('porto_alegre', 1, null) !!}
        </div>
        {!! Form::textarea('porto_alegre_texto', null, ['id' => 'porto_alegre_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('porto_velho', 'Porto Velho') !!}
    {!! Form::hidden('porto_velho', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('porto_velho', 1, null) !!}
        </div>
        {!! Form::textarea('porto_velho_texto', null, ['id' => 'porto_velho_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('recife', 'Recife') !!}
    {!! Form::hidden('recife', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('recife', 1, null) !!}
        </div>
        {!! Form::textarea('recife_texto', null, ['id' => 'recife_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('rio_branco', 'Rio Branco') !!}
    {!! Form::hidden('rio_branco', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('rio_branco', 1, null) !!}
        </div>
        {!! Form::textarea('rio_branco_texto', null, ['id' => 'rio_branco_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('rio_de_janeiro', 'Rio de Janeiro') !!}
    {!! Form::hidden('rio_de_janeiro', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('rio_de_janeiro', 1, null) !!}
        </div>
        {!! Form::textarea('rio_de_janeiro_texto', null, ['id' => 'rio_de_janeiro_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('salvador', 'Salvador') !!}
    {!! Form::hidden('salvador', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('salvador', 1, null) !!}
        </div>
        {!! Form::textarea('salvador_texto', null, ['id' => 'salvador_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('sao_luis', 'São Luís') !!}
    {!! Form::hidden('sao_luis', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('sao_luis', 1, null) !!}
        </div>
        {!! Form::textarea('sao_luis_texto', null, ['id' => 'sao_luis_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('sao_paulo', 'São Paulo') !!}
    {!! Form::hidden('sao_paulo', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('sao_paulo', 1, null) !!}
        </div>
        {!! Form::textarea('sao_paulo_texto', null, ['id' => 'sao_paulo_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('teresina', 'Teresina') !!}
    {!! Form::hidden('teresina', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('teresina', 1, null) !!}
        </div>
        {!! Form::textarea('teresina_texto', null, ['id' => 'teresina_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('vitoria', 'Vitória') !!}
    {!! Form::hidden('vitoria', 0) !!}
    <div class="input-group">
        <div class="input-group-addon">
            {!! Form::checkbox('vitoria', 1, null) !!}
        </div>
        {!! Form::textarea('vitoria_texto', null, ['id' => 'vitoria_texto', 'class' => 'form-control ckeditor', 'data-editor' => 'cleanBrMapa']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
