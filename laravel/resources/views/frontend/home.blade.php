@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
            @if($banner->texto)
            <div class="texto"><span>{{ $banner->texto }}</span></div>
            @endif
            @if($banner->link)
            <a href="{{ $banner->link }}">SAIBA MAIS &raquo;</a>
            @endif
        </div>
        @endforeach
        <div class="cycle-pager"></div>
    </div>

@endsection
