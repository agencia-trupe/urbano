@extends('frontend.common.template')

@section('content')

    <div class="servicos-show">
        <div class="titulo">
            <div class="center">
                <h1>{{ $servico->titulo }}</h1>
            </div>
        </div>
        <div class="conteudo">
            <div class="center">
                <img src="{{ asset('assets/img/servicos/ampliado/'.$servico->imagem) }}" alt="">
                <div class="texto">
                    {!! $servico->texto !!}
                </div>
            </div>
        </div>
        <div class="outros">
            <div class="center">
                <p>Outros serviços:</p>
                @foreach($servicos as $outro)
                @unless($outro->slug == $servico->slug)
                <a href="{{ route('nossos-servicos', $outro->slug) }}">
                    <img src="{{ asset('assets/img/servicos/thumb/'.$outro->imagem) }}" alt="">
                    <span>{{ $outro->titulo }}</span>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection
