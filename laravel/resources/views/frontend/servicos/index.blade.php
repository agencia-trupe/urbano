@extends('frontend.common.template')

@section('content')

    <div class="servicos-index">
        <div class="faixa">
            <div class="center">
                <h1>
                    Temos uma gestão atenta às oportunidades do mercado.<br>
                    Desenvolvemos negócios considerando todo o ciclo do empreendimento.
                </h1>
            </div>
        </div>
        <div class="thumbs">
            <div class="center">
                @foreach($servicos as $servico)
                <a href="{{ route('nossos-servicos', $servico->slug) }}">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                    <h3>{{ $servico->titulo }}</h3>
                    <p><strong>{{ $servico->chamada_titulo }}</strong></p>
                    <p>{{ $servico->chamada_texto }}</p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
