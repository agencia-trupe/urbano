@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <img src="{{ asset('assets/img/layout/img-contato.png') }}" alt="">
            <div class="box">
                O que nos move é desenvolver negócios na área imobiliária. Fale com a gente!
            </div>
            <div class="texto">
                <h2>MATRIZ</h2>
                <p>{!! $contato->endereco_matriz !!}</p>

                <h2 class="map">BRASÍLIA &middot; DF</h2>
                <p>
                    <span>
                        <?php $telefone = explode(' ', $contato->telefone_brasilia); ?>
                        {{ array_shift($telefone) }}
                        <strong>{{ implode(' ', $telefone) }}</strong>
                    </span>
                    <a href="mailto:{{ $contato->email_brasilia }}">{{ $contato->email_brasilia }}</a>
                </p>

                <h2 class="map">SÃO PAULO &middot; SP</h2>
                <p>
                    <span>
                        <?php $telefone = explode(' ', $contato->telefone_saopaulo); ?>
                        {{ array_shift($telefone) }}
                        <strong>{{ implode(' ', $telefone) }}</strong>
                    </span>
                    <a href="mailto:{{ $contato->email_saopaulo }}">{{ $contato->email_saopaulo }}</a>
                </p>
            </div>
        </div>
    </div>

@endsection
