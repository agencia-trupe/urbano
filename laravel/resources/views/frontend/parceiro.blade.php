@extends('frontend.common.template')

@section('content')

    <div class="parceiro">
        <div class="center">
            <img src="{{ asset('assets/img/layout/img-parceiros.png') }}" alt="">
            <div class="box">
                O Mercado Imobiliário continua em expansão e nosso conhecimento técnico e comercial ajudará a garantir o sucesso do empreendimento.
            </div>
            <div class="texto">
                <p>A URBANO busca investidores, imobiliárias e corretores locais como parceiros interessados em ampliar e fomentar negócios consistentes.</p>
                <h2>Projetos assertivos, completos, rentáveis e seguros.</h2>
                <p>Seja nosso parceiro!</p>
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <input type="submit" value="ENVIAR">
                    <div id="form-contato-response"></div>
                </form>
            </div>
        </div>
    </div>

@endsection
