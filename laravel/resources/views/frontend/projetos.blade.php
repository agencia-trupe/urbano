@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <div class="titulo">
            <div class="center">
                <h1>
                    Nossos projetos são criativos e harmônicos, visando a rentabilidade do negócio.<br>
                    Essa é a marca registrada da Urbano.
                </h1>
            </div>
        </div>
        <div class="projetos-thumbs desenvolvimento">
            <div class="center">
                <h2>PROJETOS EM DESENVOLVIMENTO</h2>
                <div>
                    @foreach($desenvolvimento as $projeto)
                    <a href="#" class="projeto" data-galeria="{{ $projeto->id }}">
                        <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                        <div class="texto">
                            <p>{{ $projeto->titulo }}</p>
                            <p>{!! $projeto->descricao !!}</p>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="projetos-thumbs desenvolvido">
            <div class="center">
                <h2>PROJETOS DESENVOLVIDOS</h2>
                <div>
                    @foreach($desenvolvido as $projeto)
                    <a href="#" class="projeto" data-galeria="{{ $projeto->id }}">
                        <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                        <div class="texto">
                            <p>{{ $projeto->titulo }}</p>
                            <p>{!! $projeto->descricao !!}</p>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="mapa">
            <div class="center">
                <div class="texto">
                    <span>Nossa experiência no mercado imobiliário possibilitou fomentar negócios em 20 estados + Distrito Federal</span>
                </div>
                <div class="map">
                    <div id="map"></div>
                    <script>
                        function initMap() {
                            var center = {lat: -15.467349, lng: -54.1935629};
                            var pinIcon = document.getElementsByTagName('base')[0].href + '/assets/img/layout/pin-mapa.png';
                            var map = new google.maps.Map(document.getElementById('map'), {
                                center: center,
                                zoom: 4,
                                scrollwheel: false,
                                mapTypeControl: false,
                                streetViewControl: false
                            });
                            google.maps.event.addDomListener(window, 'resize', function() {
                                map.setCenter(center);
                            });

                            var coordenadasEstados = {
                                'aracaju': { lat: -11.0059756, lng: -37.2443926 },
                                'belo_horizonte': { lat: -19.9026612, lng: -44.1044816 },
                                'belem': { lat: -1.3021166, lng: -48.7342484 },
                                'boa_vista': { lat: -8.0555337, lng: -34.8976279 },
                                'brasilia': { lat: -15.7217173, lng: -48.0786679 },
                                'campo_grande': { lat: -20.4810434, lng: -54.7759655 },
                                'cuiaba': { lat: -15.6143962, lng: -56.1120197 },
                                'curitiba': { lat: -25.4950497, lng: -49.4302291 },
                                'florianopolis': { lat: -27.610016, lng: -48.7654658 },
                                'fortaleza': { lat: -3.7913486, lng: -38.5894839 },
                                'goiania': { lat: -16.6958285, lng: -49.4446991 },
                                'joao_pessoa': { lat: -7.1496617, lng: -34.9536583 },
                                'macapa': { lat: 0.101772, lng: -51.237293 },
                                'maceio': { lat: -9.5934144, lng: -35.8271107 },
                                'manaus': { lat: -3.0446529, lng: -60.1075362 },
                                'natal': { lat: -5.7999146, lng: -35.2924576 },
                                'palmas': { lat: -10.2600295, lng: -48.4876664 },
                                'porto_alegre': { lat: -30.1087954, lng: -51.317335 },
                                'porto_velho': { lat: -8.7565392, lng: -63.9251196 },
                                'recife': { lat: -8.0464253, lng: -35.0728257 },
                                'rio_branco': { lat: -9.9863232, lng: -67.9013094 },
                                'rio_de_janeiro': { lat: -22.9109866, lng: -43.7292211 },
                                'salvador': { lat: -12.9015866, lng: -38.5602395 },
                                'sao_luis': { lat: -2.5606303, lng: -44.3283348 },
                                'sao_paulo': { lat: -23.6821591, lng: -46.876186 },
                                'teresina': { lat: -5.0937244, lng: -42.8815104, },
                                'vitoria': { lat: -20.2821776, lng: -40.356383 },
                            };

                            var markers = [
                                @foreach($estados as $estado)
                                    @if($marcadores->{$estado} == 1)
                                    {
                                        estado: '{{ $estado }}',
                                        text: '{!! str_replace("\r", "", str_replace("\n", "", $marcadores->{$estado.'_texto'})) !!}'
                                    },
                                    @endif
                                @endforeach
                            ];

                            var markersArray = [], infoWindowArray = [];
                            for (i in markers) {
                                markersArray.push(new google.maps.Marker({
                                        position: coordenadasEstados[markers[i].estado],
                                        map: map,
                                        icon: pinIcon
                                }));
                                infoWindowArray.push(new google.maps.InfoWindow({
                                    content: markers[i].text,
                                    maxWidth: 250
                                }));
                                if (markers[i].text != '') {
                                    google.maps.event.addListener(markersArray[i], 'click', (function(i) {
                                        return function() {
                                            infoWindowArray[i].open(map, markersArray[i]);
                                        }
                                    })(i));
                                }
                            }
                        }
                    </script>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeObT34PkELMVpJx0UNZhr8-u_R6jlDbI&callback=initMap"></script>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden">
        @foreach($desenvolvido as $projeto)
        @foreach($projeto->imagens as $imagem)
        <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $projeto->id }}"></a>
        @endforeach
        @endforeach
        @foreach($desenvolvimento as $projeto)
        @foreach($projeto->imagens as $imagem)
        <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $projeto->id }}"></a>
        @endforeach
        @endforeach
    </div>

@endsection
