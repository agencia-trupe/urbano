    <footer>
        <div class="center">
            <img src="{{ asset('assets/img/layout/logo-urb_rodape.png') }}" alt="">
            <nav>
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('quem-somos') }}">QUEM SOMOS</a>
                <a href="{{ route('nossos-servicos') }}">NOSSOS SERVIÇOS</a>
                <a href="{{ route('projetos') }}">PROJETOS</a>
                <a href="{{ route('parceiro') }}">SEJA NOSSO PARCEIRO</a>
                <a href="{{ route('contato') }}">CONTATO</a>
            </nav>
            <div class="social">
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook"></a>
                @endif
                @if($contato->instagram)
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram"></a>
                @endif
            </div>
            <div class="locais">
                <a href="{{ route('contato') }}">BRASÍLIA &middot; DF</a>
                <a href="{{ route('contato') }}">SÃO PAULO &middot; SP</a>
            </div>
            <div class="copyright">
                <p>© {{ date('Y') }} Urbano Desenvolvimento Imobiliário - Todos os direitos reservados.</p>
            </div>
        </div>
    </footer>
