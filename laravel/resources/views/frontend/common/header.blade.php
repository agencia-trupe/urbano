    <header @if(Route::currentRouteName() === 'home') class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav id="desktop">
                <a href="{{ route('quem-somos') }}" @if(Route::currentRouteName() == 'quem-somos') class="active" @endif>QUEM SOMOS</a>
                <a href="{{ route('nossos-servicos') }}" @if(Route::currentRouteName() == 'nossos-servicos') class="active" @endif>NOSSOS SERVIÇOS</a>
                <a href="{{ route('projetos') }}" @if(Route::currentRouteName() == 'projetos') class="active" @endif>PROJETOS</a>
                <a href="{{ route('parceiro') }}" @if(Route::currentRouteName() == 'parceiro') class="active" @endif>SEJA NOSSO PARCEIRO</a>
                <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>CONTATO</a>
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
        <nav id="mobile">
            <a href="{{ route('quem-somos') }}" @if(Route::currentRouteName() == 'quem-somos') class="active" @endif>QUEM SOMOS</a>
            <a href="{{ route('nossos-servicos') }}" @if(Route::currentRouteName() == 'nossos-servicos') class="active" @endif>NOSSOS SERVIÇOS</a>
            <a href="{{ route('projetos') }}" @if(Route::currentRouteName() == 'projetos') class="active" @endif>PROJETOS</a>
            <a href="{{ route('parceiro') }}" @if(Route::currentRouteName() == 'parceiro') class="active" @endif>SEJA NOSSO PARCEIRO</a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>CONTATO</a>
        </nav>
    </header>
