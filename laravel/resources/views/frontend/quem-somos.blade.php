@extends('frontend.common.template')

@section('content')

    <div class="quem-somos">
        <div class="faixa" style="background-image:url('{{ asset('assets/img/quem-somos/'.$quemSomos->imagem) }}')">
            <div class="center">
                <h1>{{ $quemSomos->chamada }}</h1>
                <div class="texto">{!! $quemSomos->texto !!}</div>
            </div>
        </div>
        <div class="quem-somos-contato">
            <div class="center">
                <h3>{{ $quemSomos->chamada_contato }}</h3>
                <p>{{ $quemSomos->texto_contato }}</p>
            </div>
        </div>
    </div>

@endsection
